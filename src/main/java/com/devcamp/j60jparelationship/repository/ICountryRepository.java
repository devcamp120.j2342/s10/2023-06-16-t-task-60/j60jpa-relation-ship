package com.devcamp.j60jparelationship.repository;

import com.devcamp.j60jparelationship.model.CCountry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ICountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
}
